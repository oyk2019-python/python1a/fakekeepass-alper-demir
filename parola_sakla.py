parolalar = []                                                                              # Parolaların saklanacağı listenin içi boş olarak yaratılması.
while True:                                                                                 # Temel döngümüzü yazılım kapatılana kadar veya crash olana kadar çalışacak şekilde tanımlıyoruz.
    print('Bir işlem seçin')                                                                # -
    print('1- Parolaları Listele')                                                          # Bir üst ve bir alt satırlarda print ile kullanıcıya yapabileceği işlemleri listeliyoruz.
    print('2- Yeni Parola Kaydet')                                                          # -
    islem = input('Ne Yapmak İstiyorsun :')                                                 # Kullanıcıdan yukarıdaki işlemlerden birini seçip giriş yapmasını istiyor ve girdiyi "islem" değişkenine atıyoruz.
    if islem.isdigit():                                                                     # Kullanıcının girdisi olan "islem" değişkeninin rakam olup olmadığını kontrol ediyoruz.
        islem_int = int(islem)                                                              # Eğer "islem" değişkenimiz rakam ise bunu "islem_int" olarak int'e convert ediyoruz.
        if islem_int not in [1, 2]:                                                         # "islem_int" değişkenimiz içerisinde 1 ya da 2 int değerini barındırmıyorsa "Hatalı işlem girişi" uyarısı bastırıyoruz. Çünkü sadece 2 adet işlemimiz var.
            print('Hatalı işlem girişi')                                                    # -
            continue                                                                        # Uyarı basıldıktan sonra döngünün başa sarmasını istiyoruz.
        if islem_int == 2:                                                                  # Kullanıcımızın işlem listesinden seçtiği rakam 2 ise bu "if" in altında yer alan işlemleri uygulatacağız.

            girdi_ismi = input('Bir girdi ismi ya da web sitesi adresi girin :')            # Kullanıcıdan bir girdi ismi alıyorz (Örn: gmail)

            kullanici_adi = input('Kullanici Adi Girin :')                                  # Kullanıcıdan bir kullanıcı adı alıyoruz (Örn: rodeno)

            parola = input('Parola :')                                                      # Kullannıcıdan bir parola yazmasını istiyoruz.
            parola2 = input('Parola Yeniden :')                                             # Kullanıcıdan yukarıda yazdığı parolayı yeniden yazmasını istiyoruz. (Bir sonraki adımlarda bu ikisinin aynı olup olmadığını kontrol edeceğiz.)
            eposta = input('Kayitli E-posta :')                                             # Kullanıcıdan kayıtlı e-posta adresini yazmasını istiyoruz.
            gizlisorucevabi = input('Gizli Soru Cevabı :')                                  # Kullanıcıdan Gizli Soru cevabını yazmasını istiyoruz.
            if kullanici_adi.strip() == '':                                                 # -
                print('kullanici_adi girmediz')                                             # Eğer "kullanıcı_adi" için girilen input'da boşluk var ise döngüyü başa döndürüyoruz.
                continue                                                                    # -
            if parola.strip() == '':                                                        # -
                print('parola girmediz')                                                    # Eğer "parola" için girilen input'da boşluk var ise döngüyü başa döndürüyoruz.
                continue                                                                    # -
            if parola2.strip() == '':                                                       # -
                print('parola2 girmediz')                                                   # Eğer "parola2" için girilen input'da boşluk var ise döngüyü başa döndürüyoruz.
                continue                                                                    # -
            if eposta.strip() == '':                                                        # -
                print('eposta girmediz')                                                    # Eğer "e-posta" için girilen input'da boşluk var ise döngüyü başa döndürüyoruz.
                continue                                                                    # -
            if gizlisorucevabi.strip() == '':                                               # -
                print('gizlisorucevabi girmediz')                                           # Eğer "gizlisorucevabi" için girilen input'da boşluk var ise döngüyü başa döndürüyoruz.
                continue                                                                    # -
            if girdi_ismi.strip() == '':                                                    # -
                print('Girdi ismi girmediz')                                                # Eğer "girdi_ismi" için girilen input'da boşluk var ise döngüyü başa döndürüyoruz.
                continue                                                                    # -
            if parola2 != parola:                                                           # -
                print('Parolalar eşit değil')                                               # Parolaların birbirine eşit olup olmadığını burada kontrol ediyoruz. Değilse döngüyü başa döndürüyoruz.
                continue                                                                    #

            yeni_girdi = {                                                                  # Yukarıda yer alan tüm kontrollerden geçtikten sonra kullanıcı doğru girişler yaptıysa girdilerini "yeni_girdi" değişkenine atıyoruz.
                'girdi_ismi': girdi_ismi,                                                   # -
                'kullanici_adi': kullanici_adi,                                             # -
                'parola': parola,                                                           # -
                'eposta': eposta,                                                           # -
                'gizlisorucevabi': gizlisorucevabi,                                         # -
            }
            parolalar.append(yeni_girdi)                                                    # "yeni_girdi" değişkenimizin içindeki kullanıcıdan aldığımız verileri listemize ekliyoruz.
            continue                                                                        # Ve "2" kodlu işlem başarılı bir şekilde tamamlandığı için döngüyü başa döndürebiliriz.
        elif islem_int == 1:                                                                # Kullanıcımızın işlem listesinden seçtiği rakam 1 ise bu "if" in altında yer alan işlemleri uygulatacağız.
            alt_islem_parola_no = 0                                                         # Listeye ekleyeceğimiz parolaları numaralandırabilmek için bir değişken tanımlıyoruz.
            for parola in parolalar:                                                        # Bir for döngüsü başlatıp kullanıcıya sistemde kayıtlı olan parolaları listeleyeceğiz.
                alt_islem_parola_no += 1                                                    # Parolaları numaralandırırken for döngüsü içerisinde numaranın sürekli +1 olarak ilerlemesini sağlıyoruz.
                print('{parola_no} - {girdi}'.format(parola_no=alt_islem_parola_no, girdi=parola.get('girdi_ismi')))    # Parola numarası ve Girdi adını kullanıcıya listeliyoruz. Bunu yaparken format dan faydalanıyoruz.
            alt_islem = input('Yukarıdakilerden hangisi ?: ')                                                           # Kullanıcıdan ona listelediğimiz girdilerden birini seçmesini istiyoruz.
            if alt_islem.isdigit():                                                                                     # Kullanıcının girdisinin rakam olup olmadığını kontrol ediyoruz.
                if int(alt_islem) < 1 and len(parolalar) - 1 < int(alt_islem):                                          # Kullanıcının parola listelerken listelemek istediği girdi numarasının 1 den ufak ve parolalar listemizden uzun bir girdi olmasını istemiyoruz.
                    print('Hatalı parola seçimi')                                                                       # -
                    continue                                                                                            # Parola seçimi hatalıysa döngü başa dönüyor.
                parola = parolalar[int(alt_islem) - 1]                                                                  # Parola şeklinde bir değişken oluşturuyoruz ve bu değişkene parolalar listemizdeki kullanıcının seçtiği girdiyi atıyoruz.
                print('{kullanici}\n{parola}\n{gizli}\n{eposta}'.format(                                                # Kullanıcıya seçtiği girdiyi print ve get kullanarak listeliyoruz.
                    kullanici=parola.get('kullanici_adi'),
                    parola=parola.get('parola'),
                    eposta=parola.get('eposta'),
                    gizli=parola.get('gizlisorucevabi'),
                ))
                continue                                                                                                # Kullanıcının seçebileceği işlemler tamamlandığı için döngüyü başa döndürüyoruz.

    print('Hatalı giriş yaptınız')                                                                                      # While döngümüzün şartları dışında bir işlem gerçekleşirse Hatalı giriş yaptınız uyarısı veriyoruz.
